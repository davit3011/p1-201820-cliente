package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.DoubleLinkedList;
import model.vo.Station;
import model.vo.Trip;

public class LinkedListTest<T> {
	@Test
		public void testLinkedList()
		{
			DoubleLinkedList singly = new DoubleLinkedList<>(); 
			assertTrue(singly.estaVacia());
			assertEquals(0, singly.size()); 
			singly.add("123");
			assertFalse(singly.estaVacia());
			assertEquals(1, singly.size());
		}

}

