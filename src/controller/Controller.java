package controller;

import model.data_structures.IDoublyLinkedList;
import model.data_structures.IQueue;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

import java.time.LocalDateTime;

import API.IManager;


public class Controller {
    private static IManager manager = new Manager();
public int darCantidadViajes()
{
	return manager.darCantidadViajes();
}
public int darCantidadStations()
{
	return manager.darCantidadStations();
}
    public static IQueue<Trip> A1(LocalDateTime fechaInicial, LocalDateTime fechaFinal){
        return manager.A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
    }

    public static IDoublyLinkedList<Bike> A2(LocalDateTime fechaInicial, LocalDateTime fechaFinal){
        return manager.A2BicicletasOrdenadasPorNumeroViajes(fechaInicial, fechaFinal);
    }

    public IDoublyLinkedList<Trip> A3(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.A3ViajesPorBicicletaPeriodoTiempo(bikeId, fechaInicial, fechaFinal);
    }

    public IDoublyLinkedList<Trip> A4(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.A4ViajesPorEstacionFinal(endStationId, fechaInicial, fechaFinal);
    }

    public IQueue<Station> B1(LocalDateTime fechaComienzo) {
        return manager.B1EstacionesPorFechaInicioOperacion(fechaComienzo);
    }

    public IDoublyLinkedList<Bike> B2(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.B2BicicletasOrdenadasPorDistancia(fechaInicial, fechaFinal);
    }

    public IDoublyLinkedList<Trip> B3(int bikeId, int tiempoMaximo, String genero) {
        return manager.B3ViajesPorBicicletaDuracion(bikeId, tiempoMaximo, genero);
    }

    public IDoublyLinkedList<Trip> B4(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
        return manager.B4ViajesPorEstacionInicial(startStationId, fechaInicial, fechaFinal);
    }
    public static void C1cargar(String dataTrips, String dataStations){
    	manager.C1cargar(dataTrips, dataStations);
    }
    public IQueue<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal){
    	return manager.C2ViajesValidadosBicicleta(bikeId, fechaInicial, fechaFinal);
    }
    public  IDoublyLinkedList<Bike> C3BicicletasMasUsadas(int topBicicletas){
		return manager.C3BicicletasMasUsadas(topBicicletas);
    }
    public IDoublyLinkedList<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal){
    	return manager.C4ViajesEstacion(StationId, fechaInicial, fechaFinal);
    }
}
