package model.data_structures;

import java.util.EmptyStackException;
import java.util.Iterator;

public class Stack<G> implements IStack
{
	public class Node<G>
	{
	    private G element;
	    private Node<G> next;

	    /**
	     * Creates a node with null references to its element and next node
	     */
	    public Node()
	    {
	        this(null, null);
	    }

	    /**
	     * Creates a node with the given element and next node
	     */
	    public Node(G e, Node<G> n)
	    {
	        element = e;
	        next = n;
	    }

	    /**
	     * Accessor methods
	     */
	    public G getElement()
	    {
	        return element;
	    }

	    public Node<G> getNext()
	    {
	        return next;
	    }

	    /**
	     * Modifier methods
	     */
	    public void setElement(G newElem)
	    {
	        element = newElem;
	    }

	    public void setNext(Node<G> newNext)
	    {
	        next = newNext;
	    }
	}
	    protected Node<G> top;   
	    protected int size;      

	    public Stack()
	    {
	        top = null;
	        size = 0;
	    }
	    public int getSize() {
	        return size;
	    }
   @Override
	    public boolean isEmpty() {
	        if(top == null)
	            return true;
	        return false;
	    }

	    public G top() throws EmptyStackException {
	        if(isEmpty())
	            throw new EmptyStackException();
	        return top.getElement();
	    }
	    
	    public void push(G element) {
	        Node<G> v = new Node<G>(element, top);
	        top = v;
	        size++;
	    }
	    @Override
	    public G pop() throws EmptyStackException {
	        if(isEmpty())
	            throw new EmptyStackException();
	        G temp = top.getElement();
	        top = top.getNext();
	        size--;
	        return temp;
	    }
		@Override
		public Iterator iterator() {
			// TODO Auto-generated method stub
			return null;
		}
		@Override
		public void enqueue(Object elem) {
			// TODO Auto-generated method stub
			
		}
		@Override
		public Object dequeue() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}





