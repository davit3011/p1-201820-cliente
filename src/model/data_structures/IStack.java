package model.data_structures;

import java.util.Iterator;

public interface IStack<T> extends Iterable<T>{
	
	public void enqueue(T elem);

	public T dequeue();
    
	public int getSize();
    
	public boolean isEmpty();

    public Iterator<T> iterator();
    
    public T pop();

	
	
}
