package model.data_structures;

import java.util.Iterator;

public class Iterador<E> implements Iterator<E> {

	private Nodo<E> actual;

	public Iterador(Nodo<E> primerNodo)
	{
		actual =primerNodo;
	}

	public boolean hasNext()
	{
		return actual!= null;
	}
	public E next()
	{


		E valor = actual.darElemento();
		actual= actual.darSiguiente();
		return valor;
	}
}
