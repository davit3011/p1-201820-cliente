package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Time;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import com.opencsv.CSVReader;

import API.IManager;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.IQueue;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

public class Manager implements IManager {
	//Ruta del archivo de datos 2017-Q1
	public static final String TRIPS_Q1 = "data/Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	public static final String TRIPS_Q2 = "data/Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	public static final String TRIPS_Q3 = "data/Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	public static final String TRIPS_Q4 = "data/Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de stations 2017-Q1-Q2
	public static final String STATIONS_Q1_Q2 = "data/Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	public static final String STATIONS_Q3_Q4 = "data/Divvy_Stations_2017_Q3Q4.csv";
	private DoubleLinkedList<Trip> listaViajes;
	private DoubleLinkedList<Station> listaStation;
	private CSVReader reader2;
	private CSVReader reader1;
	private int cantidadViajesCargados;
	private int cantidadStationsCargadas;
	private Queue<Trip> cola;
	private Stack<Trip> pila;
	

	public Manager()
	{
		listaViajes=new DoubleLinkedList<>();
		listaStation= new DoubleLinkedList<>();
		cola = new Queue<>();
		pila =  new Stack<>();
		
	}
	public void C1cargar(String rutaTrips, String rutaStations) 
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
		String[] lineaLeer1;
		String[] lineaLeer2;
		try {
			reader2 = new CSVReader(new FileReader(rutaStations));
			reader1 = new CSVReader(new FileReader(rutaTrips));
			lineaLeer1=reader1.readNext();
			lineaLeer2=reader2.readNext();
			while(((lineaLeer2 = reader2.readNext())!=null))
			{
				try {	

					int id=Integer.parseInt(lineaLeer2[0]);
					String nombre= lineaLeer2[1];
					String ciudad=lineaLeer2[2];
					double latitude= Double.parseDouble(lineaLeer2[3]);
					double longitude= Double.parseDouble(lineaLeer2[4]);
					int dpcap=Integer.parseInt(lineaLeer2[5]);
					String onDate=lineaLeer2[6];
					Station station=new Station(id, nombre, ciudad, latitude, longitude, dpcap, onDate);
					listaStation.add(station);
					cantidadStationsCargadas++;
				}
				catch(Exception e){
					System.out.println("ERROR POR " + e.getMessage());
				}
				while(((lineaLeer1 = reader1.readNext())!=null)) {
					try {

						int pid=Integer.parseInt(lineaLeer1[0]);
						String startTime= lineaLeer1[1];
						String endTime= lineaLeer1[2];
						int bikeId=Integer.parseInt(lineaLeer1[3]);
						int tripDuration=Integer.parseInt(lineaLeer1[4]);
						int fromStationId=Integer.parseInt(lineaLeer1[5]);
						String fromStationName=lineaLeer1[6];
						int toStationId=Integer.parseInt(lineaLeer1[7]);
						String toStationName=lineaLeer1[8];
						String userType=lineaLeer1[9];
						String gender=lineaLeer1[10];
						String birth=lineaLeer1[11];
						Trip station2=new Trip(pid, startTime, endTime, bikeId, tripDuration, fromStationId, fromStationName,toStationId, toStationName, userType, gender, birth );
						listaViajes.add(station2);
						cantidadViajesCargados++;

					}
					catch(Exception e)
					{
						System.out.println("error por" + e.getMessage());
					}
				}

			}




		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}
	public int darCantidadStations()
	{
		return cantidadStationsCargadas;
	}
	public int darCantidadViajes()
	{
		return cantidadViajesCargados;
	}
	/**
	 * M�todo que me permite ordenar una lista de viajes
	 * @param nalga
	 * @return
	 */
	public Queue<Trip> ordenarColaViajes(Queue<Trip> nalga)
	{
		Trip [] alterno = new Trip[nalga.size()];
		for(int i =0 ; i<nalga.size(); i++)
		{
			alterno[i]= nalga.dequeue();
		}
		MergeSort.sort(alterno);
		for(int i =0 ; i<nalga.size(); i++)
		{
			nalga.enqueue(alterno[i]);
		}
		return nalga;
		
	}
	
	public Queue<Station> ordenarColaEstacionesMerge (Queue<Station> nalga)
	{
		Station [] alterno = new Station[nalga.size()];
		for(int i =0 ; i<nalga.size(); i++)
		{
			alterno[i]= nalga.dequeue();
		}
		MergeSort.sort(alterno);
		for(int i =0 ; i<nalga.size(); i++)
		{
			nalga.enqueue(alterno[i]);
		}
		return nalga;
	}


	public IQueue<Trip> A1ViajesEnPeriodoDeTiempo(LocalDateTime fechaInicial, LocalDateTime fechaFinal) 
	{
		Queue<Trip> colita = new Queue<Trip>();
		Queue<Trip>devolver = new Queue<Trip>();
		
			
			
			Instant instante1 = fechaInicial.atZone(ZoneId.of("America/Chicago")).toInstant();
			Date output =Date.from(instante1);
			Instant instante2 = fechaFinal.atZone(ZoneId.of("America/Chicago")).toInstant();
			Date fecha2 = Date.from(instante2);
			
			for(Trip actual :listaViajes)
			{
				int respuesta1 = output.compareTo(actual.getStartTime());
				int respuesta2 = fecha2.compareTo(actual.getStopTime());
				//System.out.println(actual.getBikeId());
				if(respuesta1<=0 && respuesta2>=0)
				{
					colita.enqueue(actual);
				}
			}
			return colita;
	}
	
	public Station darEstacionActual(int pIdStation)
	{
		Station devolver = null;
		for(Station st : listaStation)
		{
			if(st.getStationId()==pIdStation)
			{
				devolver = st;
			}
		}
		return devolver;
	}

	public IDoublyLinkedList<Bike> A2BicicletasOrdenadasPorNumeroViajes(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		DoubleLinkedList<Bike> resp= new DoubleLinkedList<Bike>();
		IQueue<Trip> viajes =A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
		//se buscan los trips con el mismo bike id, y solo se agrega uno a la lista
		
		for (int i = 0; i < viajes.size(); i++) 
		{
			Trip actual=viajes.dequeue();
			
			int idActual=actual.getBikeId();
			int total=1;
			int duracionTotal=actual.getTripDuration();
			double distanciaTotal=
			getDistance(darEstacionActual(actual.getStartStationId()).getLatitude(),
					darEstacionActual(actual.getStartStationId()).getLongitude(),
					darEstacionActual(actual.getEndStationId()).getLatitude(),
					darEstacionActual(actual.getEndStationId()).getLongitude());
			
			
			for(int j=0; j< viajes.size(); j++)
			{
				Trip actual2=viajes.dequeue();
				if(idActual==actual2.getBikeId())
				{
					total++;
					duracionTotal+=actual2.getTripDuration();
					distanciaTotal+=
							getDistance(darEstacionActual(actual2.getStartStationId()).getLatitude(),
							darEstacionActual(actual2.getStartStationId()).getLongitude(),
							darEstacionActual(actual2.getEndStationId()).getLatitude(),
							darEstacionActual(actual2.getEndStationId()).getLongitude());
					
				}
				else
				{
					viajes.enqueue(actual2);
				}
			}
			Bike biky= new Bike(idActual, total, distanciaTotal, duracionTotal);
			resp.add(biky);
			
		}
		
		return resp;
		
	}
	

	public IDoublyLinkedList<Trip> A3ViajesPorBicicletaPeriodoTiempo(int bikeId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		DoubleLinkedList<Trip> colita = new DoubleLinkedList<Trip>();
		
		IQueue<Trip> hola = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
		for(Trip actual : hola)
		{
			if(actual.getBikeId()== bikeId)
			{
				colita.add(actual);
			}
		}
		return colita;

}

	public IDoublyLinkedList<Trip> A4ViajesPorEstacionFinal(int endStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
      DoubleLinkedList<Trip> colaRespuesta = new DoubleLinkedList<Trip>();
		
		IQueue<Trip> resultadoHoras = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
		for(Trip actual : resultadoHoras)
		{
			if(actual.getEndStationId() == endStationId)
			{
				colaRespuesta.add(actual);
			}
		}
		return colaRespuesta;
	}

	public IQueue<Station> B1EstacionesPorFechaInicioOperacion(LocalDateTime fechaComienzo) {
		
		Queue<Station> lista =  new Queue<>();
		Instant instante1 = fechaComienzo.atZone(ZoneId.of("America/Chicago")).toInstant();
		Date output =Date.from(instante1);
		for(Station actual : listaStation)
		{
			Date horaComienzo = actual.getStartDate();
			if(output.compareTo(horaComienzo) <=0)
			{
				lista.enqueue(actual);
			}
		}
		ordenarColaEstacionesMerge(lista);
		return lista;
	}
	public double getDistance (double lat1, double lon1, double lat2, double lon2)  {   
		// TODO Auto-generated method stub    
		final int R = 6371*1000; // Radious of the earth in meters    
		Double latDistance = Math.toRadians(lat2-lat1);     
		Double lonDistance = Math.toRadians(lon2-lon1);     
Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance/2) *  Math.sin(lonDistance/2);   
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));     Double distance = R * c;  
		return distance;
	}

	public IDoublyLinkedList<Bike> B2BicicletasOrdenadasPorDistancia(LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		DoubleLinkedList<Bike> resp = new DoubleLinkedList<>();
		IQueue<Trip> b = A1ViajesEnPeriodoDeTiempo(fechaInicial, fechaFinal);
		int total=1;
		for (int i = 0; i < b.size(); i++) 
		{
			Trip actual=b.dequeue();
			double distancia = getDistance(darEstacionActual(actual.getStartStationId()).getLatitude(),
					darEstacionActual(actual.getStartStationId()).getLongitude(),
					darEstacionActual(actual.getEndStationId()).getLatitude(),
					darEstacionActual(actual.getEndStationId()).getLongitude());
			int idActual = actual.getBikeId();

			for(int j=0; j< b.size(); j++)
			{
				Trip actual2=b.dequeue();
				if(idActual==actual2.getBikeId())
				{
					total++;
					distancia+= getDistance(darEstacionActual(actual2.getStartStationId()).getLatitude(),
							darEstacionActual(actual2.getStartStationId()).getLongitude(),
							darEstacionActual(actual2.getEndStationId()).getLatitude(),
							darEstacionActual(actual2.getEndStationId()).getLongitude());
				}
				else
				{
					b.enqueue(actual2);
				}
			}
			Bike biky= new Bike(idActual, total, distancia, 0);
			resp.add(biky);
		}

		return resp;
	}

	public IDoublyLinkedList<Trip> B3ViajesPorBicicletaDuracion(int bikeId, int tiempoMaximo, String genero) {
		return null;
	}

	public IDoublyLinkedList<Trip> B4ViajesPorEstacionInicial(int startStationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		DoubleLinkedList <Trip> ready = new DoubleLinkedList<Trip>();
		
		Instant instante1 = fechaInicial.atZone(ZoneId.systemDefault()).toInstant();
		Date output =Date.from(instante1);
		Instant instante2 = fechaFinal.atZone(ZoneId.systemDefault()).toInstant();
		Date fecha2 = Date.from(instante2);
		
		for(Trip actual :listaViajes)
		{
			int respuesta1 = output.compareTo(actual.getStartTime());
			int respuesta2 = actual.getStopTime().compareTo(fecha2);
			System.out.println(actual.getBikeId());
			if(respuesta1<=0 && respuesta2>=0 && actual.getStartStationId() == startStationId )
			{
				ready.add(actual);
			}
		}
		return ready;

}


	public IDoublyLinkedList<Bike> C3BicicletasMasUsadas(int topBicicletas) {
		Stack<Bike> ciclon = new Stack<Bike>();
		DoubleLinkedList respuesta = new DoubleLinkedList<>();
		Bike [] arreglo = new Bike[topBicicletas];
		LocalDateTime fechaInicial = LocalDateTime.of(2017, 3, 9, 00, 00);
		LocalDateTime fechaFinal = LocalDateTime.of(2017, 3, 10, 00, 00);		
		IDoublyLinkedList<Bike> a = A2BicicletasOrdenadasPorNumeroViajes(fechaInicial, fechaFinal);
		int i=0;
		for(Bike cicla : a)
		{
			ciclon.push(cicla); 
		}
		Bike maximo = null;
		Bike comparar = null;
		while(!ciclon.isEmpty() && respuesta.size() < topBicicletas){

			maximo = (ciclon.pop());

			for(i=0;i<ciclon.getSize();i++)
			{
				comparar=ciclon.pop();
				if(maximo.getTotalDuration() < comparar.getTotalDuration()){
					ciclon.push(maximo);
					maximo=comparar;
				}
			}
			respuesta.add(maximo);
		}	
		return respuesta;
	}

	public IDoublyLinkedList<Trip> C4ViajesEstacion(int StationId, LocalDateTime fechaInicial, LocalDateTime fechaFinal) {
		return null;
	}
	@Override
	public IQueue<Trip> C2ViajesValidadosBicicleta(int bikeId, LocalDateTime fechaInicial,LocalDateTime fechaFinal) {
		Queue<Trip> validacion =  new Queue<>();
		Queue<Trip> inconsistente = new Queue<>();
		IDoublyLinkedList<Trip> datos = A3ViajesPorBicicletaPeriodoTiempo(bikeId,fechaInicial, fechaFinal);
		for(Trip anaconda : datos)
		{
			if(validacion.isEmpty()){
				validacion.enqueue(anaconda);
			}else{
				Trip lultimo = validacion.dequeue();
				if(lultimo.getEndStationName().equals(anaconda.getStartStationName())){
					validacion.enqueue(anaconda);
				}
				else{
					validacion.enqueue(anaconda);
					inconsistente.enqueue(anaconda);
					inconsistente.enqueue(lultimo);
				}
			}
		}
		return inconsistente;
	}
}