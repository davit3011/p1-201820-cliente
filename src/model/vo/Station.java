package model.vo;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

public class Station implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private String stationCity;
	private double stationLatitude;
	private double stationLongitude;
	private int stationCapacity;
	private Date stationDate;

	public Station(int stationId, String stationName,String pStationCity,double pStationLongitude,double pStationLatitude,int pStationCapacity, String startDate) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.stationDate = setFecha(startDate);
		this.stationCity = pStationCity;
		this.stationLatitude = pStationLatitude;
		this.stationLongitude = pStationLongitude;
		this.stationCapacity = pStationCapacity;



	}

	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stub
		return 0;
	}
	public int getStationCapacity()
	{
		return stationCapacity;
	}
	public double getLongitude()
	{
		return stationLongitude;
	}
	public double getLatitude()
	{
		return stationLatitude;	
	}
	public String getStationCity()
	{
		return stationCity;
	}
	public Date getStartDate() {
		return stationDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}
	public Date setFecha(String pFecha)
	{
		Calendar calendario = Calendar.getInstance();
		String all [] = pFecha.split(" ");
		String fecha [] = all[0].split("/");
		String hora [] = all[1].split(":");

		int mes = Integer.parseInt(fecha[0]);
		int dia = Integer.parseInt(fecha[1]);
		int anio = Integer.parseInt(fecha[2]);

		int hour = Integer.parseInt(hora[0]);
		int minuto = Integer.parseInt(hora[1]);
		int segundo =  Integer.parseInt(hora[2]);
		calendario.set(anio, mes, dia, hour, minuto, segundo);
		Date fechaFinal = calendario.getTime();
		return fechaFinal;
	}

}
